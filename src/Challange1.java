import java.util.Scanner;

public class Challange1 {
    public static void main(String[] args) {
        String ulang = "Y";
        while (ulang.equals("Y")) {
            Scanner calculator = new Scanner(System.in);

            System.out.println("---------------------------");
            System.out.println("Kalkulator penghitung luas dan volum");
            System.out.println("---------------------------");
            System.out.println("Menu");
            System.out.println("1. Hitung Luas");
            System.out.println("2. Hitung Volum");
            System.out.println("0. Tutup Aplikasi");

            System.out.print("Masukan pilihan 1/2/0 : ");
            int hasil = calculator.nextInt();
            if (hasil == 1) {
                System.out.println("---------------------------");
                System.out.println("Pilih bidang yang akan dihitung");
                System.out.println("---------------------------");
                System.out.println("1. Persegi");
                System.out.println("2. lingkaran");
                System.out.println("3. segitiga");
                System.out.println("4. persegi panjang");
                System.out.println("0. kembali ke menu sebelumnya");
                System.out.print("Masukan pilihan 1/2/3/4/0 : ");
                int pilihan1 = calculator.nextInt();
                switch (pilihan1) {
                    case 1:
                        luasPersegi();
                        break;
                    case 2:
                        luasLingkaran();
                        break;
                    case 3:
                        luasSegitiga();
                        break;
                    case 4:
                        luasPersegiPanjang();
                        break;
                    case 0:
                        ulang = "Y";
                        break;
                    default:
                        System.out.println("tidak ada");
                        break;
                }
            } else if (hasil == 2) {
                System.out.println("---------------------------");
                System.out.println("Pilih volume yang akan dihitung");
                System.out.println("---------------------------");
                System.out.println("1. kubus");
                System.out.println("2. balok");
                System.out.println("3. tabung");
                System.out.println("0. kembali ke menu sebelumnya");
                System.out.print("Masukan pilihan 1/2/3/0 : ");
                int pilihan2 = calculator.nextInt();
                switch (pilihan2) {
                    case 1:
                        kubus();
                        break;
                    case 2:
                        balok();
                        break;
                    case 3:
                        tabung();
                        break;
                    case 0:
                        ulang = "Y";
                        break;
                    default:
                        System.out.println("tidak ada");
                        break;
                }

            } else if(hasil == 0) {
                ulang="T";
            }else{
                System.out.println("Pilihan yang anda masukan salah, harap masukan pilihan sesuai yang ada di console");
            }

        }
    }


    private static void luasPersegi(){
        Scanner sisiPersegi = new Scanner(System.in);
        System.out.println("---------------------------");
        System.out.println("Anda memilih persegi");
        System.out.println("---------------------------");
        System.out.print("Masukan sisi : ");
        double intSisi = sisiPersegi.nextDouble();
        System.out.println("Processing...");
        double result = intSisi * intSisi;
        System.out.println("Luas Persegi adalah "+ result);


        System.out.println("tekan apa saja untuk untuk kembali ke menu utama");
        try
        {
            System.in.read();
        }
        catch(Exception e)
        {}

    }

    private static void luasPersegiPanjang(){
        Scanner persegiPanjang = new Scanner(System.in);
        System.out.println("---------------------------");
        System.out.println("Anda memilih persegi panjang");
        System.out.println("---------------------------");

        System.out.print("Masukan Panjang : ");
        double panjang = persegiPanjang.nextDouble();

        System.out.print("Masukan Lebar : ");
        double lebar = persegiPanjang.nextDouble();

        double result = panjang * lebar;
        System.out.println("Processing ...");
        System.out.println("Luas Persegi panjang adalah "+ result);
        System.out.println("tekan apa saja untuk untuk kembali ke menu utama");
        try
        {
            System.in.read();
        }
        catch(Exception e)
        {}
    }

    private static void luasSegitiga(){
        Scanner segitiga = new Scanner(System.in);
        System.out.println("---------------------------");
        System.out.println("Anda memilih segitiga");
        System.out.println("---------------------------");

        System.out.print("Masukan alas : ");
        double alas = segitiga.nextDouble();

        System.out.print("Masukan Tinggi : ");
        double tinggi = segitiga.nextDouble();

        double result = 0.5 * alas * tinggi;
        System.out.println("Processing ...");
        System.out.println("Luas segitiga adalah "+ result);
        System.out.println("tekan apa saja untuk untuk kembali ke menu utama");
        try
        {
            System.in.read();
        }
        catch(Exception e)
        {}
    }

    private static void luasLingkaran(){
        Scanner lingkaran = new Scanner(System.in);
        System.out.println("---------------------------");
        System.out.println("Anda memilih lingkaran");
        System.out.println("---------------------------");

        System.out.print("Masukan jari-jari : ");
        double jariJari = lingkaran.nextDouble();



        double result = 3.14 * jariJari;
        System.out.println("Processing ...");
        System.out.println("Luas Lingkaran adalah "+ result);
        System.out.println("tekan apa saja untuk untuk kembali ke menu utama");
        try
        {
            System.in.read();
        }
        catch(Exception e)
        {}
    }

    private static void kubus(){
        Scanner kubus = new Scanner(System.in);
        System.out.println("---------------------------");
        System.out.println("Anda memilih Kubus");
        System.out.println("---------------------------");

        System.out.print("Masukan sisi : ");
        double sisi = kubus.nextDouble();
        System.out.println("Processing...");
        double result = sisi * sisi * sisi;
        System.out.println("Luas Volume Kubus adalah "+ result);


        System.out.println("tekan apa saja untuk untuk kembali ke menu utama");
        try
        {
            System.in.read();
        }
        catch(Exception e)
        {}
    }

    private static void balok(){
        Scanner balok = new Scanner(System.in);
        System.out.println("---------------------------");
        System.out.println("Anda memilih Balok");
        System.out.println("---------------------------");

        System.out.print("Masukan Panjang : ");
        double panjang = balok.nextDouble();
        System.out.print("Masukan Lebar : ");
        double lebar = balok.nextDouble();
        System.out.print("Masukan Tinggi : ");
        double tinggi = balok.nextDouble();
        System.out.println("Processing...");
        double result = panjang * lebar * tinggi;
        System.out.println("Luas Volume Balok adalah "+ result);


        System.out.println("tekan apa saja untuk untuk kembali ke menu utama");
        try
        {
            System.in.read();
        }
        catch(Exception e)
        {}
    }

    private static void tabung(){
        Scanner tabung = new Scanner(System.in);
        System.out.println("---------------------------");
        System.out.println("Anda memilih volume tabung");
        System.out.println("---------------------------");

        System.out.print("Masukan tinggi : ");
        double tinggi = tabung.nextDouble();
        System.out.print("Masukan Jari-jari");
        double jariJari = tabung.nextDouble();
        System.out.println("Processing...");
        double result = 3.14 * tinggi * jariJari;
        System.out.println("Luas Volume Tabung adalah "+ result);


        System.out.println("tekan apa saja untuk untuk kembali ke menu utama");
        try
        {
            System.in.read();
        }
        catch(Exception e)
        {}
    }
}
